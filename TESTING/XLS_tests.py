import unittest
from openpyxl import Workbook

from . import excel_tricks

class TestsXLS(unittest.TestCase):

    def setUp(self):
        filepath ='C:/Users\Jirka\JM_Python\Excely\TESTING'
        wb1 = Workbook()
        wb2 = Workbook()
        wb1.save(filepath,'test1.xlsx')
        wb1.save(filepath,'test2.xlsx')


        self.Etest=excel_tricks.ExctractXls('test1.xlsx','test2.xlsx')
        print(self.Etest.file_read)

    def test_coordinates_from_string(self):
        result=self.Etest.coordinates_from_string(start_position='A2', end_position='B5')
        self.assertEqual(result,(1,2,2,5))     

if __name__ == '__main__':
    unittest.main()
        