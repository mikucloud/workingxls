import os
from openpyxl import load_workbook
from openpyxl import Workbook
from openpyxl.utils import coordinate_from_string, column_index_from_string

#print(os.getcwd())

class ExctractXls:
    def __init__(self,filename_read,filename_write):
        self.file_read = load_workbook(filename_read)
        self.file_write = load_workbook(filename_write)
        self.filename_read = filename_read
        self.filename_write = filename_write
        


    def set_sheet_read(self,read_sheet_name):        
        
        if read_sheet_name in self.file_read.sheetnames:
            self.sheet_read=self.file_read[read_sheet_name]
        else :            
            print('There is no such a sheetname in file', self.file_read,',please enter any valid sheet name')

    def set_sheet_write(self,write_sheet_name):        
        
        if write_sheet_name in self.file_write.sheetnames:
            self.sheet_write=self.file_write[write_sheet_name]
        else :            
            print('There is no such a sheetname in file', self.file_read,',please enter any valid sheet name')


    # def check_matrix_dimensions(data):
    #     length = len(data[0])
    #     for r in len(data):
    #         if length != len(r):
    #             return False
    #     return True

    # def data_dimensions(data):
    #     rows=len(data)
    #     columns=len(data[0])
    #     return(rows,columns)

    def coordinates_from_string(self,start_position=None, end_position=None):
        
        if start_position==None or end_position==None:
            raise RuntimeError("Start and End cell have to be given")
        
        #convert cell marking in string e.i ['A4'] to coordinates row=1 col=4
        start_pos = coordinate_from_string(start_position) # returns e.i.('A',4)
        start_col = column_index_from_string(start_pos[0]) # returns ei 1
        start_row = start_pos[1]

        end_pos = coordinate_from_string(end_position) # returns e.i.('A',4)
        end_col = column_index_from_string(end_pos[0]) # returns ei 1
        end_row = end_pos[1]
        #import pdb; pdb.set_trace()
        
        if start_col>=end_col or start_row>=end_row:
            raise RuntimeError("Start pos must be lower than edn position")
        

        return start_col,start_row,end_col,end_row

    def check_extr_insert_area(self):
        if len(self.rows_read)==len(self.rows_write) or len(self.columns_read)==len(self.columns_write):
            return True
        else:
            return False          



    "exctract data from given xls file in given area an store it in memory as a list of lists"
    def extract_data(self, start_position=None, end_position=None):      
        
        start_col,start_row,end_col,end_row=self.coordinates_from_string(start_position, end_position)
        #print(start_col,start_row,end_col,end_row)


        self.rows_read=[r for r in range(start_row,end_row+1)]
        self.columns_read=[c for c in range(start_col,end_col+1)]

        #print('from',self.rows_read, self.columns_read)

        self.extracted_data=[]
        extracted_data_row=[]

        try:
            for r in self.rows_read:
                for c in self.columns_read:
                    column_value=self.sheet_read.cell(row=r,column=c).value
                    if column_value is None:
                        column_value=0
                    extracted_data_row.append(column_value)
                    print(extracted_data_row,r,c)
                self.extracted_data.append(extracted_data_row[:])
                extracted_data_row.clear()
        except IndexError:
            print('Wrong range of rows or columns in extracted area')

    def insert_data(self, start_position=None, end_position=None):
        
        start_col,start_row,end_col,end_row=self.coordinates_from_string(start_position, end_position)
        #print(start_col,start_row,end_col,end_row)

        self.rows_write=[r for r in range(start_row,end_row+1)]
        self.columns_write=[c for c in range(start_col,end_col+1)]

        self.check_extr_insert_area()

        #print('to',self.rows_write, self.columns_write)
        i=0
        j=0

        try:
            for r in self.rows_write:
                for c in self.columns_write:
                    self.sheet_write.cell(row=r,column=c).value=self.extracted_data[i][j]
                    #print(self.sheet_write.cell(row=r,column=c).value,i,j)
                    j+=1
                i+=1
                j=0
                    
        except IndexError:
           print('Wrong range of rows or columns in insert area')

        self.file_write.save(self.filename_write)
            
    def get_data(self):
        #print(self.extracted_data)
        return self.extracted_data
                    

    def create_XLS(self,filename_out):
        wb=Workbook()
        sheet=wb.active
        wb.save(filename_out)

#wb.save('xlsjm.xlsx')

# filename_read_path='C:/Users/Jirka/JM_Python/Excely/newFile.xlsx'
# filename_write_path='C:/Users/Jirka/JM_Python/Excely/vlozit.xlsx'

filename_read_path='C:/Users/Jirka/JM_Python/Excely/piloty/C16.xlsx'
filename_write_path='C:/Users/Jirka/JM_Python/Excely/piloty/SectionsC16.xlsx'

# filename_read_path='C:/Users/Jirka/JM_Python/Excely/piloty/SES1.xlsx'
#filename_write_path='C:/Users/Jirka/JM_Python/Excely/piloty/SES2.xlsx'

#create_XLS(filename_out)
#import pdb; pdb.set_trace()
if __name__ == '__main__':
    E1=ExctractXls(filename_read_path,filename_write_path)
    # E1.set_sheet_read('vystup')
    # E1.set_sheet_write('Sections')

    E1.set_sheet_read('List1')
    E1.set_sheet_write('List1')

    E1.extract_data('D6', 'F362')
    E1.insert_data('D6', 'F362')
    print('extracted list is :',E1.extracted_data)


   


